import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class Numbers extends StatelessWidget {
  final String num;
  final Color cl;
  Numbers({
    Key key,
    this.num,
    this.cl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 20,
      width: 20,
      decoration:
          BoxDecoration(color: cl, borderRadius: BorderRadius.circular(10)),
      child: Center(
        child: Text(num,
            textAlign: TextAlign.center,
            style: GoogleFonts.poppins(
                color: Colors.black,
                fontSize: 12,
                fontWeight: FontWeight.normal)),
      ),
    );
  }
}

class Btn extends StatelessWidget {
  final String name;

  final String color;

  Btn({
    Key key,
    this.name,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: EdgeInsets.only(
        left: 10,
        right: 10,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        gradient: LinearGradient(colors: [
          HexColor("#FE904B"),
          HexColor("#FB724C"),
        ]),
      ),
      child: Center(
        child: Text(name,
            style: GoogleFonts.poppins(
              color: Colors.white,
              fontSize: 17,
              fontWeight: FontWeight.w700,
            )),
      ),
    );
  }
}
