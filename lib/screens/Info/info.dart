import 'package:dog_app/constants.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class Info extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        background(),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 500,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
            ),
            child: Column(
              children: [
                Text(
                  "Alex Murray",
                  style: GoogleFonts.poppins(
                    color: HexColor("#2B2B2B"),
                    fontSize: 28,
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.center,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "5\$/hr",
                      style: GoogleFonts.poppins(
                        color: HexColor("#2B2B2B"),
                        fontSize: 13,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Container(
                      width: 2,
                      height: 13,
                      color: HexColor("#A1A1A1"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      "10 km",
                      style: GoogleFonts.poppins(
                        color: HexColor("#2B2B2B"),
                        fontSize: 13,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Container(
                      width: 2,
                      height: 13,
                      color: HexColor("#A1A1A1"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      "4.4",
                      style: GoogleFonts.poppins(
                        color: HexColor("#2B2B2B"),
                        fontSize: 13,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Container(
                      width: 2,
                      height: 13,
                      color: HexColor("#A1A1A1"),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      "450 walks",
                      style: GoogleFonts.poppins(
                        color: HexColor("#2B2B2B"),
                        fontSize: 13,
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  width: 350,
                  height: 1,
                  color: HexColor("#A1A1A1"),
                ),
                Row(
                  children: [],
                )
              ],
            ),
          ),
        ),
      ],
    ));
  }
}

class background extends StatelessWidget {
  const background({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
              ),
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage("assets/IMAGE.png"),
              ),
            ),
          ),
        ),
        Expanded(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
              ),
            ),
          ),
        )
      ],
    );
  }
}
