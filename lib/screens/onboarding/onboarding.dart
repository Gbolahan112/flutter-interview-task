import 'package:dog_app/constants.dart';
import 'package:dog_app/screens/Info/info.dart';
import 'package:dog_app/screens/SignUp/signup.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class Onboarding extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage("assets/ONBOARDING PICTURE.png"),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.only(
            left: 20,
            right: 20,
            top: 70,
            bottom: 10,
          ),
          child: Column(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Row(
                  children: [
                    Image.asset("assets/woo dog.png"),
                    Image.asset("assets/paw.png"),
                  ],
                ),
              ),
              SizedBox(
                height: 445,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Numbers(
                    num: "1",
                    cl: Colors.white,
                  ),
                  SizedBox(
                    width: 1,
                  ),
                  Container(
                    color: Colors.white,
                    height: 2,
                    width: 6,
                  ),
                  SizedBox(
                    width: 1,
                  ),
                  Numbers(num: "2", cl: HexColor("#404040")),
                  SizedBox(
                    width: 1,
                  ),
                  Container(
                    color: Colors.white,
                    height: 2,
                    width: 6,
                  ),
                  Numbers(num: "3", cl: HexColor("#404040")),
                ],
              ),
              SizedBox(
                height: 35,
              ),
              Text(
                "Too tired to walk your dog? \n Let’s help you",
                style: GoogleFonts.poppins(
                  color: HexColor("#8A877D"),
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 35,
              ),
              GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return Login();
                        },
                      ),
                    );
                  },
                  child: Btn(name: "Join our community")),
              SizedBox(
                height: 35,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Already a member? ",
                    style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w700,
                        color: Colors.white),
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Text(
                      "Sign in",
                      style: TextStyle(
                          color: HexColor("#FE904B"),
                          fontSize: 13,
                          fontWeight: FontWeight.w700,
                          decoration: TextDecoration.underline),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
