import 'package:dog_app/constants.dart';
import 'package:dog_app/screens/Home/home.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: HexColor("#E5E5E5"),
      body: GestureDetector(
        onTap: () {},
        child: Padding(
          padding: EdgeInsets.only(
            left: 20,
            right: 20,
            top: 70,
            bottom: 10,
          ),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Image.asset("assets/back.png"),
                ),
                SizedBox(
                  height: size.height * 0.1,
                ),
                Text(
                  "Let’s  start here",
                  style: GoogleFonts.poppins(
                    color: HexColor("#2B2B2B"),
                    fontSize: 34,
                  ),
                  textAlign: TextAlign.center,
                ),
                Text(
                  "Fill in your details to begin",
                  style: GoogleFonts.poppins(
                      color: HexColor("#7A7A7A"), fontSize: 22),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 34,
                ),
                Container(
                  height: 70,
                  padding: EdgeInsets.only(
                    left: 15,
                    right: 10,
                    top: 10,
                    bottom: 10,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: HexColor("#F0F0F0"),
                  ),
                  child: TextFormField(
                    validator: (val) {
                      return val.isEmpty || val.length < 11
                          ? "Enter Phone 11+ characters"
                          : null;
                    },
                    decoration: InputDecoration(
                      hintText: "FullName",
                      hintStyle: GoogleFonts.poppins(
                        color: HexColor("#26200B").withOpacity(0.5),
                        fontSize: 12,
                      ),
                      border: InputBorder.none,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 70,
                  padding: EdgeInsets.only(
                    left: 15,
                    right: 10,
                    top: 10,
                    bottom: 10,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: HexColor("#F0F0F0"),
                  ),
                  child: TextFormField(
                    validator: (val) {
                      return val.isEmpty || val.length < 11
                          ? "Enter Phone 11+ characters"
                          : null;
                    },
                    decoration: InputDecoration(
                      hintText: "Email",
                      hintStyle: GoogleFonts.poppins(
                        color: HexColor("#26200B").withOpacity(0.5),
                        fontSize: 12,
                      ),
                      border: InputBorder.none,
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 70,
                  padding: EdgeInsets.only(
                    left: 15,
                    right: 10,
                    top: 10,
                    bottom: 10,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: HexColor("#F0F0F0"),
                  ),
                  child: TextFormField(
                    validator: (val) {
                      return val.isEmpty || val.length < 11
                          ? "Enter Phone 11+ characters"
                          : null;
                    },
                    decoration: InputDecoration(
                      hintText: "password",
                      hintStyle: GoogleFonts.poppins(
                        color: HexColor("#26200B").withOpacity(0.5),
                        fontSize: 12,
                      ),
                      suffixIcon: IconButton(
                        icon: Icon(
                          // Based on passwordVisible state choose the icon
                          Icons.visibility,
                          size: 30,

                          color: HexColor("#828282"),
                        ),
                      ),
                      border: InputBorder.none,
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                GestureDetector(
                      onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return Home();
                        },
                      ),
                    );
                  },
                  child: Btn(
                    name: "Sign up",
                  ),
                ),
                SizedBox(
                  height: size.height * 0.2,
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    "By signing in, I agree with Terms of Use and Privacy Policy",
                    style: GoogleFonts.poppins(
                      color: HexColor("#B0B0B0"),
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
