import 'package:dog_app/constants.dart';
import 'package:dog_app/screens/Home/Scroll.dart';
import 'package:dog_app/screens/Home/bottomnavbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBarTravel(),
      body: Padding(
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
          top: 70,
          bottom: 10,
        ),
        child: Column(
          children: [
            Row(
              children: <Widget>[
                RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: "HOME\n",
                        style: GoogleFonts.poppins(
                          color: HexColor("#2B2B2B"),
                          fontSize: 34,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      TextSpan(
                        text: "Explore dog walkers",
                        style: GoogleFonts.poppins(
                          color: HexColor("#B0B0B0"),
                          fontSize: 15,
                        ),
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Align(
                    alignment: Alignment.topLeft,
                    child: Btn(
                      name: "Book to walk",
                    )),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Image.asset(
              "assets/SEARCH FIELD.png",
              width: double.infinity,
            ),
            SizedBox(
              height: 20,
            ),
            Drow(
              name1: "Near you",
              name2: "See all",
            ),
            SizedBox(
              height: 10,
            ),
            NreaYou(),
            Drow(
              name1: "Suggested",
              name2: "See all",
            ),
            SizedBox(
              height: 10,
            ),
            Suggested(),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}

class Drow extends StatelessWidget {
  final String name1;
  final String name2;

  Drow({
    Key key,
    this.name1,
    this.name2,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          name1,
          style: GoogleFonts.poppins(
            color: HexColor("#2B2B2B"),
            fontSize: 34,
            fontWeight: FontWeight.w700,
          ),
        ),
        Spacer(),
        Text(
          name2,
          style: GoogleFonts.poppins(
            color: HexColor("#2B2B2B"),
            fontSize: 15,
            fontWeight: FontWeight.w400,
            decoration: TextDecoration.underline,
          ),
        ),
      ],
    );
  }
}
