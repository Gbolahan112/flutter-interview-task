import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class NreaYou extends StatelessWidget {
  const NreaYou({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      physics: ScrollPhysics(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          thescroll(
            img: "assets/images/photo.png",
            txt1: "Mason York\n",
            txt2: "7 km from you",
            txt3: "\$3/h",
          ),
          SizedBox(
            width: 18,
          ),
          thescroll(
            img: "assets/images/photo4.png",
            txt1: "Mark Greene\n",
            txt2: "14 km from you",
            txt3: "\$3/h",
          ),
        ],
      ),
    );
  }
}

class Suggested extends StatelessWidget {
  const Suggested({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      physics: ScrollPhysics(),
      child: Row(
        children: <Widget>[
          thescroll(
            img: "assets/images/photo3.png",
            txt1: "Mark Greene\n",
            txt2: "2 km from you",
            txt3: "\$3/h",
          ),
          SizedBox(
            width: 16,
          ),
          thescroll(
            img: "assets/images/photo2.png",
            txt1: "Trina Kain\n",
            txt2: "4 km from you",
            txt3: "\$3/h",
          ),
        ],
      ),
    );
  }
}

class thescroll extends StatelessWidget {
  final String img;
  final String txt1;
  final String txt2;
  final String txt3;

  thescroll({
    Key key,
    this.img,
    this.txt1,
    this.txt2,
    this.txt3,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      color: Colors.white,
      width: size.width * 0.5,
      height: 194,
      child: Column(
        children: [
          Container(
            width: size.width * 0.5,
            height: 134,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(img),
              ),
            ),
          ),
          SizedBox(
            height: 2,
          ),
          Padding(
            padding: EdgeInsets.only(
              left: 10,
              right: 10,
              bottom: 10,
            ),
            child: Row(
              children: <Widget>[
                RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: txt1,
                        style: GoogleFonts.poppins(
                          color: HexColor("#2B2B2B"),
                          fontSize: 17,
                        ),
                      ),
                      TextSpan(
                        text: txt2,
                        style: GoogleFonts.poppins(
                          color: HexColor("#A1A1A1"),
                          fontSize: 15,
                        ),
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Container(
                    padding: EdgeInsets.only(
                      left: 10,
                      right: 10,
                      top: 5,
                      bottom: 5,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: HexColor("#2B2B2B"),
                    ),
                    child: Center(
                      child: Text(txt3,
                          style: GoogleFonts.poppins(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                          )),
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
