import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';

class BottomNavigationBarTravel extends StatefulWidget {
  @override
  _BottomNavigationBarTravelState createState() =>
      _BottomNavigationBarTravelState();
}

class _BottomNavigationBarTravelState extends State<BottomNavigationBarTravel> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  var bottomNavStyle = GoogleFonts.poppins(
    fontSize: 12,
    fontWeight: FontWeight.w500,
    color: Colors.black,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 76.4,
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 2,
            blurRadius: 15,
            offset: Offset(0, 5))
      ]),
      child: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: _selectedIndex == 0
                ? new Image.asset(
                    'assets/Home.jpg',
                  )
                : new Image.asset(
                    'assets/Home.jpg',
                  ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: _selectedIndex == 1
                ? Icon(
                    Icons.group,
                    size: 32,
                  )
                : Icon(
                    Icons.group,
                    size: 32,
                  ),
            label: 'Moments',
          ),
          BottomNavigationBarItem(
            icon: _selectedIndex == 2
                ? new Image.asset(
                    'assets/Send.png',
                    color: Colors.orange,
                  )
                : new Image.asset(
                    'assets/Send.png',
                  ),
            label: 'Chat',
          ),
          BottomNavigationBarItem(
              icon: _selectedIndex == 3
                  ? Icon(
                      Icons.person,
                      size: 32,
                    )
                  : Icon(
                      Icons.person,
                      size: 32,
                    ),
              label: 'Profile',
              tooltip: 'jdsjd'),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        backgroundColor: Colors.transparent,
        type: BottomNavigationBarType.fixed,
        selectedFontSize: 12,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        elevation: 0,
      ),
    );
  }
}
